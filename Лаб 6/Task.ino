int gasSensorA = A5	// Аналоговый вихід №0 - 'sensor'
int sensorValue = 0;// Початкове значення sensorValue  = 0

void setup() 
{
  Serial.begin(9600);		
  pinMode (gasSensorA, INPUT);
}

void loop() 
{
  sensorValue = analogRead(gasSensorA);	 	// Зчитує ввід на аналоговый вивід 0 (з датчика)
  Serial.println(sensorValue, DEC);		// Зчитує отримане значення
  if (sensorValue > 700)              
  	{				
		tone(13, 1500);
	    	delay(500);
    		noTone (13);
	    	delay(500);
  	}
}
